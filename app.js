var geneticAlgorithmConstructor = require('geneticalgorithm')
const draw = require('./draw')
const mergeImages = require('merge-images');
const { Canvas, Image } = require('canvas');

function mutationFunction(phenotype) {
	// mutamos un color de uno de los cromosomas
	phenotype[randomIntFromInterval(0,phenotype.length-1)].color =
			[ 
				randomIntFromInterval(0,255),
				randomIntFromInterval(0,255),
				randomIntFromInterval(0,255)
			]

	// mutamos el tamaño de otro cromosoma
	phenotype[randomIntFromInterval(0,phenotype.length-1)].size =
		randomIntFromInterval(1000,10000)


	// Aleatoriamente añadimos o sacamos un cromosoma aleatorio
	let nuevoCromosoma = {
		color: 	[ 
			randomIntFromInterval(0,255),
			randomIntFromInterval(0,255),
			randomIntFromInterval(0,255)
		],
		size: randomIntFromInterval(1000,10000)
	}
	if(randomIntFromInterval(0,1)){
		phenotype.push(nuevoCromosoma)
	}
	else{
		if(phenotype.length > 1){
			phenotype.pop();
		}
	}

    	return phenotype
}

function crossoverFunction(phenotypeA, phenotypeB) {
	// move, copy, or append some values from a to b and from b to a

	// Apareamos la mitad de los comoromas

	for(let i = 0; i < Math.round((phenotypeA.length-1)/2) ; i++){
		phenotypeA = copyRandomColor(phenotypeA,phenotypeB);
	}

	for(let i = 0; i < Math.round((phenotypeB.length-1)/2) ; i++){
		phenotypeB = copyRandomColor(phenotypeB,phenotypeA);
	}
	
	return [ phenotypeA , phenotypeB ]
}

function fitnessFunction(phenotype) {
	
	// use your phenotype data to figure out a fitness score

	return phenotype.length+10*phenotype.filter( cromo => { return cromo.color[1] > 200 }).length
	
	

}

var firstPhenotype = [
	{color: [255,128,64], size: 72300},
	{color: [128,255,255], size: 31292},
	{color: [46,245,74], size: 11232},
	{color: [127,128,4], size: 40233},
	{color: [1,245,60], size: 8322},
]

var geneticAlgorithm = geneticAlgorithmConstructor({
    mutationFunction: mutationFunction,
    crossoverFunction: crossoverFunction,
    fitnessFunction: fitnessFunction,
    population: [ firstPhenotype ]
});






console.log("Starting with:")
console.log( firstPhenotype )
for( var i = 0 ; i < 100 ; i++ ) geneticAlgorithm.evolve()
var best = geneticAlgorithm.best()
delete best.score
console.log("Finished with:")
console.log(best)

var image = draw.createImage()
best.forEach( item => {
	draw.drawRandomIsland(image,item);
});

draw.render(image).on('finish',()=>{
	
	mergeImages(['./fondo.png', './newOut.png'],{
		Canvas: Canvas,
		Image: Image
	  })
	  .then(b64 => require("fs").writeFile("result.png", b64.replace(/^data:image\/png;base64,/, ""), 'base64', function(err) {
		console.log(err);
	  }));

})



/*
Como seria un fenotipo?
Islas de colores y tamaño variado
RGB

[{isla},{isla},{isla},{isla}...]

[{color: , size, },...]

*/


function randomIntFromInterval(min, max) { // min and max included 
	return Math.floor(Math.random() * (max - min + 1) + min);
}

function copyRandomColor(phenotypeA, phenotypeB){
	phenotypeA[randomIntFromInterval(0,phenotypeA.length-1)].color = phenotypeB[randomIntFromInterval(0,phenotypeB.length-1)].color
	return phenotypeA
}
