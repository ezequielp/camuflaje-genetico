
var fs = require('fs');
var PNG = require('pngjs').PNG;


	function drawRandomIsland(image, fenotipo){
		
		png = image
		var x,y = 0;
		// Seleccionamos pixel al azar aleatoriamente
		let start_x = Math.round(Math.random() * png.width) + 1 ;
		let start_y = Math.round(Math.random() * png.height) + 1 ;

		x = start_x;
		y = start_y;

		for (var total = 0;  fenotipo.size > total; total++ ) {
				
				var idx = (png.width * y + x) << 2;
				png.data[idx  ] = fenotipo.color[0]; // red
				png.data[idx+1] = fenotipo.color[1]; // green
				png.data[idx+2] = fenotipo.color[2]; // blue
				png.data[idx+3] = 255; // alpha (0 is transparent)

				x += randomIntFromInterval(-1,1)  
				y += randomIntFromInterval(-1,1)  
		}	

		return png;
	}

	function randomIntFromInterval(min, max) { // min and max included 
		return Math.floor(Math.random() * (max - min + 1) + min);
	  }

	function render(png){
		return png.pack().pipe(fs.createWriteStream('newOut.png'));
	};

	function createImage(){

		var png = new PNG({
			width: 100,
			height: 100,
			filterType: -1
		});

		for(let x1 = 0 ; x1 < png.width ; x1++){
			for(let y1 = 0 ; y1 < png.height ; y1++){
				var idx1 = (png.width * y1 + x1) << 2;
				png.data[idx1  ] = 255;
				png.data[idx1+1] = 255;
				png.data[idx1+2] = 255;
				png.data[idx1+3] = 255;
			}
		}

		return png; 
	}

	exports.drawRandomIsland = drawRandomIsland
	exports.createImage = createImage
	exports.render = render

